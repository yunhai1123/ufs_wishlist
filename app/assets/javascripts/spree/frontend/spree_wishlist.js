// Placeholder manifest file.
// the installer will append this file to the app vendored assets here: vendor/assets/javascripts/spree/frontend/all.js'
//= require jquery.form

(function($) {

  $.fn.addToWishAjaxForm = function() {
    console.log("wishing");
    var options = {
      dataType: 'json',
      success: function(e) {
        addAlert('success', e.message);
        $.ajax({ url: Spree.pathFor("wishlists"), success: function(data) {
          // $('#link-to-cart').html(data);
        }});
      },
      error: function(e) {
        addAlert('error', JSON.parse(e.responseText).message);
      }
    };
    return this.ajaxForm(options);
  }

  function animate($elem, action, speed, callback) {
    $elem.animate({
      height: action,
      marginBottom: action,
      marginLeft: action,
      marginRight: action,
      marginTop: action,
      opacity: action,
      paddingBottom: action,
      paddingLeft: action,
      paddingRight: action,
      paddingTop: action}, speed, callback);
  }

  function animateAlerts($alerts) {
    if ($alerts.length > 2) {
      animate($alerts.last(), 'hide', 200, function() { $(this).remove(); });
    }
    animate($alerts.first(), 'show', 240);
  }

  function addAlert(type, message) {
    // var $content = $('#content');
    // // $content.prepend('<div style="display: none" class="alert alert-' + type + '">' + notice + '</div>');
    // $content.prepend('<div id="gritter-notice-wrapper"><div id="gritter-item-1" class="gritter-item-wrapper" style=""><div class="gritter-item gritter-css3"><div class="gritter-close" style="display: none;"></div><div class="gritter-without-image"><span class="gritter-title">United Food Services</span><p' + message + '</p></div><div style="clear:both"></div></div></div></div>');
    //
    //
    // animateAlerts($content.find('#gritter-notice-wrapper'));

    $.gritter.add({
	// (string | mandatory) the heading of the notification
    	title: 'United Food Services',
    	// (string | mandatory) the text inside the notification
    	text: message
    });
  }

}(jQuery));
