Deface::Override.new(virtual_path: 'spree/shared/_products',
  name: 'add_wishlist_button_to_products_page',
  insert_bottom: '.wishlist-addon',
  text: <<-eos
    <div class="wishlist_products_panel media-cart">
    <%= form_for Spree::WishedProduct.new, html: { class: 'wishlist_products_form' } do |f| %>
      <%= f.hidden_field :variant_id, value: product.master.id %>
      <%= f.hidden_field :quantity %>
      <%= button_tag :class => 'btn btn-danger add_to_wishlist_products__button', :type => :submit do %>
                    + <span class="glyphicon glyphicon-heart"></span>
                  <% end %>
    <% end %>
      <script>
        $(function() {
          $('.wishlist_products_form').addToWishAjaxForm();
        });
      </script>
    </div>
  eos
)
